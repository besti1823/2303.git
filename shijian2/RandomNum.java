/*================================================================
*   Copyright (C) 2019Zhang_duanyun. All rights reserved.
*   
*   文件名称：RandomNum.java
*   创 建 者：Zhang_duanyun
*   创建日期：2019年09月18日
*   描    述：
*
================================================================*/

import java.util.Random;
import java.text.DecimalFormat;

public class RandomNum
{
	public static void main (String[] args)
	{
		Random generator = new Random();
		float num;
		num = generator.nextFloat()*20-10;
		DecimalFormat fmt = new DecimalFormat ("0.###");

		System.out.println("The random number: "+fmt.format(num));
	}
}

