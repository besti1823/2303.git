package Chapter19;
import java.util.Scanner;
public class Directed_ALGraph {
    class Vexs{//创建一个类型,用来表示顶点信息
        String data;//顶点的值
        Chain chain;//本顶点的链表
    }
    class Chain{//创建一个链表类型
        int id;//相邻接顶点的数组下标
        Chain next;//下一个相邻接顶点的数组下标
    }
    public static void main(String[] args) {
        directed_algraph();
    }
    public static void directed_algraph() {
        int _numNodes;
        System.out.println("请输入节点的个数");
        Scanner sc = new Scanner(System.in);
        _numNodes = sc.nextInt();
        Vexs[] vexs = new Vexs[_numNodes];//定义顶点数组
        System.out.println("请输入各个顶点的值");
        for(int i=0;i<vexs.length;i++) {//建立顶点表
            sc = new Scanner(System.in);
            String temp = sc.nextLine();
            Vexs v = new Directed_ALGraph().new Vexs();
            v.data = temp;
            vexs[i] = v;
        }
        System.out.println("输入各个顶点的出度");
        for(int i=0;i<vexs.length;i++) {
            System.out.println(vexs[i].data+"的出度:");
            sc = new Scanner(System.in);
            int temp = sc.nextInt();//temp临时存储控制台传来的出度值
            Chain head_chain = null;
            Chain current_chain = null;
            for(int j=0;j<temp;j++) {
                System.out.println("输入"+vexs[i].data+"的邻接点的数组下标：");
                sc = new Scanner(System.in);
                if(head_chain==null) {
                    head_chain = new Directed_ALGraph().new Chain();
                    head_chain.id = sc.nextInt();
                    vexs[i].chain = head_chain;
                }else {
                    current_chain = new Directed_ALGraph().new Chain();
                    current_chain.id = sc.nextInt();
                    head_chain.next = current_chain;
                    head_chain = current_chain;
                }
            }
        }
        for(int i=0;i<vexs.length;i++) {
            int num = 0;
            while(vexs[i].chain != null) {
                if(num==0) {
                    System.out.print("顶点的值"+vexs[i].data+"的邻接点为:"+vexs[i].chain.id);
                    num++;
                }
                else {
                    System.out.print(","+vexs[i].chain.id);
                }
                vexs[i].chain = vexs[i].chain.next;
            }
            System.out.println();
        }
    }
}