package List;
import java.util.Collections;
import java.util.Iterator;
import java.util.ArrayList;
public class ArrayListRl {
    public static void main(String []args){

        ArrayList list1 = new ArrayList();
        list1.add(20);
        list1.add(18);
        list1.add(23);
        list1.add(03);
        list1.add(11);
        list1.add(0,1);
        System.out.println("list1中共有" + list1.size()+ "个元素");
        System.out.println("list1中的内容: " + list1);

        ArrayList list2 = new ArrayList();
        list2.add(0);
        list2.addAll(list1);
        list2.add(99);
        System.out.println("list2中共有" + list2.size()+ "个元素");
        System.out.println("list2中的内容: " + list2 );

        ArrayList list3 =  new ArrayList();
        list3.removeAll(list1);
        System.out.println("list3中是否存在20: "+ (list3.contains(20)? "是":"否"));

        list3.add(0,"same element");
        list3.add(1,"same element");
        System.out.println("list3中共有" + list3.size()+ "个元素");
        System.out.println("list3中的内容:" + list3 );
        System.out.println("list3中第一次出现same element的索引是" + list3.indexOf("same element") );
        System.out.println("list3中最后一次出现same element的索引是" + list3.lastIndexOf("same element"));


        System.out.println("------使用Iterator接口访问list3------");
        Iterator it = list3.iterator();
        while(it.hasNext()){
            String str = (String)it.next();
            System.out.println("list3中的元素: " + list3 );
        }

        System.out.println("----修改list3中的same element为another element----");
        list3.set(0,"another element");
        list3.set(1,"another element");
        System.out.println("----将list3转为数组----");
        // Object []  array =(Object[]) list3.toArray(new   Object[list3.size()] );
        Object [] array = list3.toArray();
        for(int i = 0; i < array.length ; i ++){
            String str = (String)array[i];
            System.out.println("array[" + i + "] = "+ str);
        }

        System.out.println("----清空list3----");
        list3.clear();
        System.out.println("list3中是否为空: " + (list3.isEmpty()?"是":"否"));
        System.out.println("list3中共有" + list3.size()+ "个元素");
        //System.out.println("hello world!");

        System.out.println("-------将List1倒置-------");
        ArrayList list4=new ArrayList();
        list4.add(20);
        list4.add(18);
        list4.add(23);
        list4.add(03);
        list4.add(11);
        list4.add(0,1);
        Collections.reverse(list4);
        System.out.println(list4);
    }
}