package List;
import java.util.Collections;
import java.util.LinkedList;
public class LinkedListRl {

    public static void main(String[] args) {

        LinkedList<String> list = new LinkedList<String>();
        //将字符串元素加入到队列的尾部
        list.offer("23");
        System.out.println("List0: "+list);
        //将该字符串元素加入到栈的栈的顶部（相当于队列的头部）
        list.push("18");
        System.out.print("List1: ");
        for (String num : list) {
            System.out.print(num);
        }
        //将该字符串元素加入到队列头部(相当于栈的顶部)
        list.offerFirst("03");
        //访问并不删除队列头部元素
        System.out.println("\nPeekFirst: "+list.peekFirst());
        //将栈顶元素弹出栈(相当于删除队列头部元素)
        list.pop();
        System.out.print("List2: ");
        for (String num : list) {
            System.out.println(num);
        }
        System.out.println("List3(reverse): ");
        Collections.reverse(list);
        for (String num : list) {
            System.out.println(num);
        }
    }
}