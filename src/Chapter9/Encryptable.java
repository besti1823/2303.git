package Chapter9;

public interface Encryptable {
    public void encrypt();
    public String decrypt();
}
