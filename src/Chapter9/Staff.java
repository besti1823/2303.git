package Chapter9;

public class Staff {
    private StaffMember[] staffList;
    public Staff()
    {
        staffList = new StaffMember[6];
        staffList[0] = new Executive ("Zdy","123 Main Line","18811355692","20182303",2423.07);
        staffList[1] = new Employee ("Lcz","456 Off  Line","188lcispig1","20182311",1246.15);
        staffList[2] = new Employee ("Xry","789 Off Rocker","18888888888","20182399",1169.23);
        staffList[3] = new Hourly("Jack","678 Fifth Ave.","18866666666","20182366",10.);
        staffList[4] = new Volunteer ("Rose","987 Babe Blvd","18812341234");
        staffList[5] = new Volunteer ("Ben","321 Dud Lane","18843214321");

        ((Executive)staffList[0]).awardBonus(500.00);
        ((Hourly)staffList[3]).addHours(40);
    }

    public void payday(){
        double amount;
        for(int count=0;count<staffList.length;count++)
        {
            System.out.println(staffList[count]);
            amount = staffList[count].pay();
            if(amount==0.0)
                System.out.println("Thanks");
            else
                System.out.println("Paid:"+amount);
            System.out.println("-----------------------------------------------");
        }
    }

}
