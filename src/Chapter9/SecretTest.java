package Chapter9;

public class SecretTest {
    public static void main(String args[]){
        Secret hush=new Secret("zdy is studying!");
        System.out.println(hush);

        hush.encrypt();
        System.out.println(hush);

        hush.decrypt();
        System.out.println(hush);
    }
}
