package Chapter13;

public class SortPlayerList {
    public static void main(String[] args) {
        Contact[] players=new Contact[7];

        players[0]=new Contact("zhao","peining","20182301");
        players[1]=new Contact("sun","jiawei","20182302");
        players[2]=new Contact("zhang","duanyun","20182303");
        players[3]=new Contact("leng","chong","20182311");
        players[4]=new Contact("lu","yanjie","20182307");
        players[5]=new Contact("zheng","liyuan","20182320");
        players[6]=new Contact("zhang","duanfeng","20000000");

        Sorting.selectionSort(players);
        for(Comparable player:players)
            System.out.println(player);
    }
}
