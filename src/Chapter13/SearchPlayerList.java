package Chapter13;

import java.util.concurrent.Callable;
import java.util.zip.CheckedInputStream;

public class SearchPlayerList {
    public static void main(String[] args) {
        Contact[] players=new Contact[7];

        players[0]=new Contact("zhao","peining","20182301");
        players[1]=new Contact("sun","jiawei","20182302");
        players[2]=new Contact("zhang","duanyun","20182303");
        players[3]=new Contact("leng","chong","20182311");
        players[4]=new Contact("lu","yanjie","20182307");
        players[5]=new Contact("zheng","liyuan","20182320");
        players[6]=new Contact("zhang","duanfeng","20000000");

        Contact target=new Contact("zhang","duanyun","20182303");
        Contact found=(Contact)Searching.LinearSearch(players,target);
        if(found==null)
            System.out.println("Players was not found.");
        else
            System.out.println("Found: "+found);
    }
}
