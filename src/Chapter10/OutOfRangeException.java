package Chapter10;

public class OutOfRangeException extends Exception{
    OutOfRangeException(String message)
    {
        super(message);
    }
}
