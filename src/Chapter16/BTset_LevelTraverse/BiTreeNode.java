package Chapter16.BTset_LevelTraverse;

public class BiTreeNode {
    public String str;
    BiTreeNode leftChild,rightChild;

    public BiTreeNode(String str) {
        this.str = str;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public BiTreeNode getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(BiTreeNode leftChild) {
        this.leftChild = leftChild;
    }

    public BiTreeNode getRightChild() {
        return rightChild;
    }

    public void setRightChild(BiTreeNode rightChild) {
        this.rightChild = rightChild;
    }
}
