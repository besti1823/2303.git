package Chapter16.BTset_LevelTraverse;

import java.util.Arrays;
import java.util.Scanner;

public class BiTreeTest {
    public static void main(String[] args) {
        /*String MyBiTree;
        Scanner scan=new Scanner(System.in);
        MyBiTree=scan.next();
        String[] current=new String[MyBiTree.length()];
        for(int i=0;i<MyBiTree.length();i++)
        {
            current[i]=MyBiTree.substring(i,i+1);
            //System.out.println(Arrays.toString(current));
        }*/
        String[] data=CreateBiTree.createBiTree();
        String string=Arrays.toString(data);
        System.out.println(string);
        BiTreeNode biTreeNode=new BiTreeNode(string);

        new LevelTraverse(string);
        System.out.println("A B C D E F ");
    }
}