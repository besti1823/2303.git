package Chapter16.BTset_LevelTraverse;

import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;

public class LevelTraverse {
    public LevelTraverse(String string) {
    }

    public void LevelTraverse(String biTreeNode) {
        BiTreeNode mRoot = null;
        LevelTraverse(mRoot);
    }

    public LevelTraverse(BiTreeNode biTreeNode) {
        }

        void LevelTraverse(BiTreeNode current) //层序遍历_队列实现
        {
            LinkedList queue=new LinkedList();
            if (current != null)
            {
                queue.push(current);   //根节点进队列
            }

            while (queue.isEmpty()== false)  //队列不为空判断
            {
                System.out.println(current);
                if (current.getLeftChild()!=null)   //如果有左孩子，leftChild入队列
                {
                    queue.remove(current);
                    queue.push(current);
                    System.out.println(current);
                }
                if(current.getRightChild()!=null)
                {
                    queue.remove(current);
                    queue.push(current);
                    System.out.println(current);
                }
                queue.pop();  //已经遍历过的节点出队列
            }
        }
    /*public static void levelTraverse(String root) {
        if (root == null)
            return;
        Queue q = new ArrayDeque<String>();
        q.add(root);
        BiTreeNode cur=new BiTreeNode(root);
        while (!q.isEmpty()) {

        }
    }*/


}
