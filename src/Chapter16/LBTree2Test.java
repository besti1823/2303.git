package Chapter16;

public class LBTree2Test {
    public static void main(String[] args) {
        String[] preOrder = {"A","B","D","H","I","E","J","M","N","C","F","G","K","L"};
        String[] inOrder = {"H","D","I","B","E","M","J","N","A","F","C","K","G","L"};

        LinkedBinaryTree2 tree2303 = new LinkedBinaryTree2();
        tree2303.initTree(preOrder,inOrder);
        System.out.println(tree2303.postOrder().toString());
    }
}
