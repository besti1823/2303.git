package Chapter16;

import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedBinaryTreeTest {


    @Test
    public void getRootElement() {
        assertEquals("2","2");
    }

    @Test
    public void getRootNode() {
        assertEquals("2","2");
    }

    @Test
    public void getLeft() {
        assertEquals("0","0");
    }

    @Test
    public void getRight() {
        assertEquals("1","1");
    }

    @Test
    public void removeRightSubtree() {
        assertEquals(null,null);
    }

    @Test
    public void removeAllElements() {
    }

    @Test
    public void isEmpty() {
    }

    @Test
    public void size() {
    }

    @Test
    public void getHeight() {
    }

    @Test
    public void countLeaf() {
    }

    @Test
    public void contains() {
    }

    @Test
    public void find() {
    }

    @Test
    public void testToString() {
    }

    @Test
    public void inOrder() {
    }

    @Test
    public void preOrder() {
    }

    @Test
    public void postOrder() {
    }
}