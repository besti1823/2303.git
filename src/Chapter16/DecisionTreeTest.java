package Chapter16;
import java.io.FileNotFoundException;
import Chapter16.*;
public class DecisionTreeTest {
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("----------！！史上最准的心理测试！！---------");
        System.out.println("    想象你来到一座童话中的城堡，一层的狩猎室内陈列着昔日伯爵在非洲狩猎的动物标本，\n" +
                           "同层的黄色房间内的家具则是来自路易十六的礼物；来到二楼，映入眼帘的是温暖的火炉，\n" +
                           "左手边有一张桌子，桌边摆着3把椅子，桌上有什么？\n");
        DecisionTree expert = new DecisionTree("D:\\IDEA\\2303\\src\\Chapter16\\input.txt");
        expert.evaluate();
    }
}

