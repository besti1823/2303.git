package Chapter16;

import com.sun.source.tree.BinaryTree;

public class LinkedBinaryTreeTest2 {
    public static void main(String[] args) {
        BinaryTreeNode stA=new BinaryTreeNode("2");
        BinaryTreeNode stB=new BinaryTreeNode("0");
        BinaryTreeNode stC=new BinaryTreeNode("1");
        BinaryTreeNode stD=new BinaryTreeNode("8");
        BinaryTreeNode stE=new BinaryTreeNode("2");
        BinaryTreeNode stF=new BinaryTreeNode("3");
        BinaryTreeNode stG=new BinaryTreeNode("0");
        BinaryTreeNode stH=new BinaryTreeNode("3");
        LinkedBinaryTree tree=new LinkedBinaryTree();

        tree.root=stA;
        stA.left=stB;
        stA.right=stC;
        stB.left=stD;
        stB.right=stE;
        stC.left=stF;
        stC.right=stG;
        stD.left=stH;

        //构建二叉树
        /*LinkedBinaryTree tree0 = new LinkedBinaryTree(stH.getElement());
        LinkedBinaryTree tree1 = new LinkedBinaryTree(stG.getElement());
        LinkedBinaryTree tree2 = new LinkedBinaryTree(stF.getElement());
        LinkedBinaryTree tree3 = new LinkedBinaryTree(stE.getElement());
        LinkedBinaryTree tree4 = new LinkedBinaryTree(stD.getElement(),tree0,null);
        LinkedBinaryTree tree5 = new LinkedBinaryTree(stC.getElement(),tree2,tree1);
        LinkedBinaryTree tree6 = new LinkedBinaryTree(stB.getElement(),tree4,tree3);
        LinkedBinaryTree tree7 = new LinkedBinaryTree(stA.getElement(),tree5,tree6);
        System.out.println();*/
         //测试
        System.out.println("测试toString(): ");
        System.out.println(tree.postOrder());
        //System.out.println("测试getRight(): ");
        //System.out.println(tree.getRight());
        System.out.print("测试contains(): ");
        System.out.println(tree.contains("1"));
        System.out.print("测试preOrder(): ");
        System.out.println(tree.preOrder());
        System.out.print("测试postOrder(): ");
        System.out.println(tree.postOrder());
    }
}
