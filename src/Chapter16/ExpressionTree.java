package Chapter16;

import java.util.Iterator;
import java.util.Stack;
import java.util.StringTokenizer;


public class ExpressionTree extends LinkedBinaryTree {
    private Stack<String> opeStack;
    private Stack<LinkedBinaryTree> numStack;
    public ExpressionTree() {
        opeStack = new Stack<String>();
        numStack = new Stack<LinkedBinaryTree>();
    }
    public static String  toSuffix(String infix) {
        String result = "";
        //将字符串转换为数组
        String[] array = infix.split("\\s+");
        //存放操作数
        Stack<LinkedBinaryTree> num = new Stack();
        //存放操作符
        Stack<LinkedBinaryTree> op = new Stack();

        for (int a = 0; a < array.length; a++) {
            //如果是操作数，开始循环
            if (array[a].equals("+") || array[a].equals("-") || array[a].equals("*") || array[a].equals("/")) {
                if (op.empty()) {
                    //如果栈是空的，将数组中的元素建立新树结点并压入操作符栈
                    op.push(new LinkedBinaryTree<>(array[a]));
                } else {
                    //如果栈顶元素为+或-且数组的元素为*或/时，将元素建立新树结点并压入操作符栈
                    if ((op.peek().root.element).equals("+") || (op.peek().root.element).equals("-") && array[a].equals("*") || array[a].equals("/")) {
                        op.push(new LinkedBinaryTree(array[a]));
                    } else {
                        //将操作数栈中的两个元素作为左右孩子，操作符栈中的元素作为根建立新树
                        LinkedBinaryTree right = num.pop();
                        LinkedBinaryTree left = num.pop();
                        LinkedBinaryTree temp = new LinkedBinaryTree(op.pop().root.element, left, right);
                        //将树压入操作数栈，并将数组中的元素建立新树结点并压入操作符栈
                        num.push(temp);
                        op.push(new LinkedBinaryTree(array[a]));
                    }
                }

            } else {
                //将数组元素建立新树结点并压入操作数栈
                num.push(new LinkedBinaryTree<>(array[a]));
            }
        }
        while (!op.empty()) {
            LinkedBinaryTree right = num.pop();
            LinkedBinaryTree left = num.pop();
            LinkedBinaryTree temp = new LinkedBinaryTree(op.pop().root.element, left, right);
            num.push(temp);
        }
        //输出后缀表达式
        Iterator itr=num.pop().iteratorPostOrder();
        while (itr.hasNext()){
            result+=itr.next()+" ";
        }
        return result;
    }
}
