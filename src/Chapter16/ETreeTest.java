package Chapter16;
import Chapter14.Postfix.PostfixEvaluator;
import java.util.Scanner;

import static Chapter16.ExpressionTree.toSuffix;

public class ETreeTest {
    public static void main(String[] args) {
        String infix, again;
        int result;
        Scanner in = new Scanner(System.in);
        do {
            PostfixEvaluator evaluator = new PostfixEvaluator();
            System.out.println("输入中缀表达式(e.g 1 + 2 * 3)");
            infix = in.nextLine();
            //中缀转后缀
            String suffix =toSuffix(infix);

            //输出结果
            result = evaluator.evaluate(suffix);

            System.out.println("\n后缀表达式为 ：" + suffix);
            System.out.println("计算结果为 ："+result);

            /*System.out.println("表达树为 :");
            System.out.println(evaluator.toString());*/

            System.out.println("Continue?[Y/N]");
            again = in.nextLine();
            System.out.println();
        }
        while (again.equalsIgnoreCase("Y"));
    }
}

