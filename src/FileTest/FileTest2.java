package FileTest;

import java.io.*;

public class FileTest2 {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("C:\\Users\\lenovo\\Desktop","FileTest2.txt");
        //File file = new File("HelloWorld.txt");
//        File file1 = new File("C:\\Users\\besti\\Desktop\\FileTest\\Test\\Test");
//        file1.mkdir();
//        file1.mkdirs();

        if (!file.exists()) {
            file.createNewFile();
        }
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] hello = {'I','a','m','Z','D','Y',',','a','s','t','u','!','M','y',' ','F','i','l','e','2'};
        outputStream1.write(hello);
        outputStream1.flush();//可有可无，不执行任何操作！！！
        byte[] buffer = new byte[1024];
        String content = "";
        int flag = 0;
        InputStream inputStream2 = new FileInputStream(file);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream2);

        while ((flag = bufferedInputStream.read(buffer)) != -1) {
            content += new String(buffer, 0, flag);
        }

        System.out.println(content);
        bufferedInputStream.close();
        System.out.println("文件读结束：BufferedInputStream直接读并输出！");
    }}