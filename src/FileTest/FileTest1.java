package FileTest;

import java.io.*;

public class FileTest1 {
    public static void main(String[] args) throws IOException {
        File file = new File("C:\\Users\\lenovo\\Desktop","HelloWorld.txt");
        if(!file.exists()){
            file.createNewFile();
        }
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] hello = {'I','a','m','Z','D','Y',',','a','s','t','u','!'};
        outputStream1.write(hello);
        outputStream1.flush();//可有可无，不执行任何操作！！！

        InputStream inputStream1 = new FileInputStream(file);
        while (inputStream1.available()> 0){
            System.out.print((char) inputStream1.read()+"  ");
        }
        inputStream1.close();
        System.out.println("\n文件读写结束：OutputStream、InputStream先写后读");
    }
}

