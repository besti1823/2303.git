package FileTest;
import java.io.*;
import java.util.StringTokenizer;

public class FileComplex {
        public static void main(String[] args) throws IOException {
            File file = new File("C:\\Users\\lenovo\\Desktop","Complex.txt");
            if (!file.exists()) {
                file.createNewFile();
            }

            OutputStream outputStream = new FileOutputStream(file);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
            String content = "(2+2i)+(3+2i)";
            bufferedOutputStream.write(content.getBytes(),0,content.getBytes().length);
            bufferedOutputStream.flush();
            bufferedOutputStream.close();

            Reader reader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(reader);
            content =bufferedReader.readLine();
            System.out.println(content);

            String data1 = content.substring(1,4);
            String operation = content.substring(6, 7);
            String data2 = content.substring(8,11);
            StringTokenizer step = new StringTokenizer(data1, "+");
            int data1_1 = Integer.parseInt(step.nextToken());
            int data1_2 = Integer.parseInt(step.nextToken());
            step = new StringTokenizer(data2, "+");
            int data2_1 = Integer.parseInt(step.nextToken());
            int data2_2 = Integer.parseInt(step.nextToken());
            //System.out.println(data1);
            //System.out.println(operation);
            //System.out.println(data2);
            String result= "";
            int r=data1_1+data2_1;
            int i=data1_2+data2_2;
            result=r+"+"+i+"i";
            System.out.println(result);
            Writer writer = new FileWriter(file);
            writer.write(result);
            writer.flush();
        }
}
