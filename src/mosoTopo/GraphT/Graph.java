package mosoTopo.GraphT;

import java.util.ArrayList;

public class Graph {
    public ArrayList<String> vertexList;//存储节点
    public int[][] edges;//邻接矩阵，用来存储边
    public int numofEdges;//边的数目
    public int n;

    //初始化矩阵
    public Graph(int n) {
        vertexList = new ArrayList<String>(n);
        edges = new int[n][n];
        numofEdges = 0;
        this.n = n;
    }

    //得到节点的个数
    public int getNumofVertex() {
        return vertexList.size();
    }

    //得到边的个数
    public int getNunofEdges() {
        return numofEdges;
    }

    //返回两个节点之间的权值
    public int getWeight(String temp1, String temp2) {
        int i = vertexList.indexOf(temp1);
        int j = vertexList.indexOf(temp2);
        return edges[i][j];
    }

    //插入节点
    public void InsertVertex(String vertex) {
        vertexList.add(vertex);
    }

    //删除节点
    public void RemoveVertex(String vertex){
        vertexList.remove(vertex);
    }

    //插入边
    public void InsertEdge(int v1, int v2, int weight) {
        edges[v1][v2] = weight;
        edges[v2][v1] = weight;
        numofEdges++;
    }

    public void deleteEdge(int v1, int v2) {
        edges[v1][v2] = 0;
        numofEdges--;
    }

    public int wuxiangtu(int a) {
        int result = 0;
        for (int i = 0; i < n; i++) {
            result += edges[a][i];
        }
        return result;
    }

    public void InsertEdge2(int v1, int v2, int weight) {
        edges[v1][v2] = weight;
        numofEdges++;
    }

    public int youxiangtu(int a)
    {
        int result = 0;
        for (int i = 0; i < n; i++) {
            result += edges[a][i];
        }
        for(int i=0;i<n;i++)
        {
            result +=edges[i][a];
        }
        return  result;
    }
}
