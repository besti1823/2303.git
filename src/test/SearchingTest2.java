package test;

import cn.edu.besti.cs1823.Zdy2303.Searching;

public class SearchingTest2 {
    public static void main(String[] args) {
        Integer[] a={20,18,23,3,2303,9,2,18,11,5,19,34};
        Integer[] b={34,19,5,11,18,2,9,2303,3,23,18,20};
        Searching searching=new Searching();
        //正序
        Comparable found1=searching.LinearSearch(a,2303);//正常
        System.out.println(found1);
        Comparable found2=searching.LinearSearch(a,35);//异常
        System.out.println(found2);
        Comparable found3=searching.LinearSearch(a,34);//边界
        System.out.println(found3);
        //逆序
        Comparable found4=searching.LinearSearch(b,2303);//正常
        System.out.println(found4);
        Comparable found5=searching.LinearSearch(b,35);//异常
        System.out.println(found5);
        Comparable found6=searching.LinearSearch(b,34);//边界
        System.out.println(found6);
    }
}
