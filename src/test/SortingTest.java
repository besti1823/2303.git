package test;

import cn.edu.besti.cs1823.Zdy2303.Sorting;
import org.junit.Test;

import static org.junit.Assert.*;

public class SortingTest {

    @Test
    public void testSelectionSort() {
        //正序
        Comparable List1[]={2,3,5,9,11,18,20,21,2303};
        Sorting.selectionSort(List1);
        assertEquals(List1[1],List1[1]);
        //逆序
        Comparable List2[]={2303,21,20,18,11,9,5,3,2};
        Sorting.selectionSort(List2);
        assertEquals(List2[0],List1[0]);
        //乱序
        Comparable List3[]={20,18,3,9,2,11,5,21,2303};
        Sorting.selectionSort(List3);
        assertEquals(List3[5],List1[5]);
        /*
        //异常——空指针
        Comparable ListNull[]=null;
        Sorting.selectionSort(ListNull);
        assertEquals(ListNull[0], List1[0]);
        */

        //异常——越界
        /*Comparable ListOutOfBound[]={20,18,3,9,2,11,5,21,2303};
        Sorting.selectionSort(ListOutOfBound);
        assertEquals(ListOutOfBound[9],List1[9]);*/

    }
    @Test
    public void testinsertionSort() {
        Comparable list[] = {2,3,5,9,11,18,20,21,2303};
        // 正常测试+边界测试
        Comparable list2[] = {2303,21,20,18,11,9,5,3,2};
        Sorting.insertionSort(list2);
        assertEquals(list2[0],list[0]);
//        // 异常测试
//        Comparable listerror[] = null;
//        Sorting2.insertionSort(listerror);
//        assertEquals(listerror[0],list[0]);
    }

    @Test
    public void testbubbleSort() {
        Comparable list[] = {2,3,5,9,11,18,20,21,2303};
        // 正常测试+边界测试
        Comparable list3[] = {2303,21,20,18,11,9,5,3,2};
        Sorting.bubbleSort(list3);
        assertEquals(list3[0],list[0]);
//        // 异常测试
//        Comparable listerror[] = null;
//        Sorting2.bubbleSort(listerror);
//        assertEquals(listerror[0],list[0]);
    }

    @Test
    public void testmergeSort() {
        Comparable list[] = {2,3,5,9,11,18,20,21,2303};
        // 正常测试+边界测试
        Comparable list4[] = {2303,21,20,18,11,9,5,3,2};
        Sorting.mergeSort(list4,0,8);
        assertEquals(list4[0],list[0]);
//        // 异常测试
//        Comparable listerror[] = null;
//        Sorting2.mergeSort(listerror);
//        assertEquals(listerror[0],list[0]);
    }

    @Test
    public void testquickSort() {
        Comparable list[] = {2,3,5,9,11,18,20,21,2303};
        // 正常测试+边界测试
        Comparable list5[] = {2303,21,20,18,11,9,5,3,2};
        Sorting.quickSort(list5,0,8);
        assertEquals(list5[0],list[0]);
//        // 异常测试
//        Comparable listerror[] = null;
//        Sorting2.quickSort(listerror);
//        assertEquals(listerror[0],list[0]);
    }

    @Test
    public void testheapSort() {
        int list[] = {2,3,5,9,11,18,20,21,2303};
        // 正常测试+边界测试
        int list6[] = {2303,21,20,18,11,9,5,3,2};
        Sorting.HeapSort(list6);
        assertEquals(list6[0],list[0]);
//        // 异常测试
//        int listerror[] = null;
//        Sorting2.HeapSort(listerror);
//        assertEquals(listerror[0],list[0]);
    }

    @Test
    public void testshellSort() {
        int list[] = {2,3,5,9,11,18,20,21,2303};
        // 正常测试+边界测试
        int list7[] = {2303,21,20,18,11,9,5,3,2};
        Sorting.ShellSort(list7,9);
        assertEquals(list7[0],list[0]);
//        // 异常测试
//        int listerror[] = null;
//        Sorting2.ShellSort(listerror,1);
//        assertEquals(listerror[0],list[0]);
    }

    @Test
    public void testBinaryTreeSort(){
        String list[] = {"1","3","6","13","18","20","23","81","9","2303"};
        BinarySearchTree tree = new LinkedBinarySearchTree();
        tree.addElement(2303);
        tree.addElement(99);
        tree.addElement(81);
        tree.addElement(23);
        tree.addElement(20);
        tree.addElement(17);
        tree.addElement(13);
        tree.addElement(6);
        tree.addElement(3);
        tree.addElement(1);
        int a = tree.size();
        String[] b = new String[a];
        for (int i = 0;i < a;i++){
            b[i] = String.valueOf(tree.findMin());
            tree.removeMin();
        }
        // 正常测试+边界测试
        assertEquals(b[0],list[0]);
//        // 异常测试
//        String[] c = null;
//        assertEquals(c[0],list[0]);
    }
}