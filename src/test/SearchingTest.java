package test;

import cn.edu.besti.cs1823.Zdy2303.Searching;
import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SearchingTest extends TestCase {
    Integer[] a={20,18,23,3,2303,9,2,18,11,5,19,34};
    Integer[] b={34,19,5,11,18,2,9,2303,3,23,18,20};
    Searching searching=new Searching();
    @Test
    public void testLinearSearch(){
        assertEquals(2303,searching.LinearSearch(a,2303));//正常
        assertEquals(2303,searching.LinearSearch(b,2303));
        assertEquals(null,searching.LinearSearch(a,35));//异常
        assertEquals(null,searching.LinearSearch(b,35));
        assertEquals(34,searching.LinearSearch(a,34));//边界
        assertEquals(20,searching.LinearSearch(b,20));
        assertEquals(9,searching.LinearSearch(a,9));
        assertEquals(9,searching.LinearSearch(b,9));
        assertEquals(23,searching.LinearSearch(a,23));
        assertEquals(23,searching.LinearSearch(b,23));
    }
    @Test
    public void testbinarySearch() {
        // 正常测试
        Comparable list[] = {1,2,3,4,5,6,7,23,456,77,2303};
        // 元素在所查找的范围内
        assertEquals(4,Searching.binarySearch(list,4));
        assertEquals(2303,Searching.binarySearch(list,2303)); //边界测试
    }

    @Test
    public void testinsertionSearch() {
        // 正常测试
        int list[] = {1,2,3,4,5,6,7,23,77,456,2303};
        // 元素在所查找的范围内
        assertEquals(1,Searching.InsertionSearch(list,2,0,10));
        assertEquals(10,Searching.InsertionSearch(list,2303,0,10)); //边界测试
    }

    @Test
    public void testfibonacciSearch() {
        // 正常测试
        int list[] = {1,23,456,77,2303};
        // 元素在所查找的范围内
        assertEquals(0,Searching.FibonacciSearch(list,4,456));
        assertEquals(4,Searching.FibonacciSearch(list,4,2303)); //边界测试
    }
}