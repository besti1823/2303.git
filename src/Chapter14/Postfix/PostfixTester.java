package Chapter14.Postfix;
/*================================================================
*   Copyright (C) 2019Zhang_duanyun. All rights reserved.
*   
*   文件名称：PostfixTester.java
*   创 建 者：Zhang_duanyun
*   创建日期：2019年10月18日
*   描    述：
*
================================================================*/

import Chapter14.Postfix.PostfixEvaluator;

import java.util.Scanner;
/**
 * Demonstrates the use of a stack to evaluate postfix expressions.
 */
public class PostfixTester
{
	public static void main(String[] args)
	{
		String expression,again;
		int result;

		Scanner in =new Scanner(System.in);

		do
		{
			PostfixEvaluator evaluator=new PostfixEvaluator();
			System.out.println("Enter a valid postfix expression:");
			expression=in.nextLine();

			result=evaluator.evaluate(expression);
			System.out.println();
			System.out.println("That expression equals"+result);

			System.out.println("Evaluate another expression[Y/N]?");
			again=in.nextLine();
			System.out.println();
		}
		while(again.equalsIgnoreCase("y"));
	}
}

