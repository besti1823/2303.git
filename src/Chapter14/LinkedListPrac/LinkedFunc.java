package Chapter14.LinkedListPrac;

import Chapter14.LinkedStack.LinearNode;
import Chapter14.StackArray.EmptyCollectionException;
import Chapter14.StackArray.StackADT;

public class LinkedFunc<T> implements StackADT<T> {
    private int count;
    private LinearNode<T> top;//设置栈顶
    private LinearNode<T> tail;
    public int[] array = null;

    //构造函数，用于初始化
    public LinkedFunc() {
        count = 0;
        top = null;
        tail = null;
    }

    @Override
    public void push(T element) {
        LinearNode<T> temp = new LinearNode<T>(element);
        temp.setNext(top);
        top = temp;
        count++;
    }

    @Override
    public void expandCapacity() {

    }


    @Override
    public T pop() throws EmptyCollectionException {

        if (isEmpty()) {
            throw new EmptyCollectionException("the stack is empty!");
        }

        T result = top.getElement();
        top = top.getNext();
        count--;

        return result;
    }

    @Override
    public T peek() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("Stack");
        }
        T result = top.getElement();

        return result;
    }

    @Override
    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String toString() {
        LinearNode current = top;
        String result = "";
        while (current != null) {
            result += current.getElement() + " ";
            current = current.getNext();
        }
        return result;
    }

    @Override
    public void addFirst(T element) {
        LinearNode<T> node = new LinearNode<T>((T) element);
        if (top == null) {
            top = node;
            tail = node;
        } else {
            node.setNext(top);
            top = node;
        }
        count++;
    }

    @Override
    public void addMiddle(int index, T element) {
        LinearNode<T> node = new LinearNode<>(element);
        LinearNode<T> current = top;
        int j = 0;
        while (current != null && j < index - 2) {
            //使指针指向index-1的位置
            current = current.getNext();
            j++;
        }
        node.setNext(current.getNext());
        current.setNext(node);
        count++;
    }

    @Override
    public void Delete(int index) {
        LinearNode<T> current = top;
        LinearNode<T> temp = top;
        if (index == 0) {
            top = top.getNext();
        } else {
            for (int i = 0; i < index - 1; i++) {
                temp = current;
                current = current.getNext();
            }
            temp.setNext(current.getNext());
        }
        count--;
    }

    public void bubbleSort() {
        LinearNode current = top;
        int temp;
        while (current != null) {
            LinearNode num = current.next;
            while (num != null) {
                if (current.num < num.num) {
                    temp = current.num;
                    current.num = num.num;
                    num.num = temp;
                }
                num = num.next;
            }
            current = current.next;
        }
    }
}