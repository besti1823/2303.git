package Chapter14.LinkedListPrac.Exp2;

import Chapter14.LinkedListPrac.LinkedFunc;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class LinkedListExp2 {
    public static void main(String[] args) throws IOException {
        Scanner scan=new Scanner(System.in);
        System.out.println("Input Number,Date,Time: ");
        String str= scan.nextLine();
        LinkedFunc queue=new LinkedFunc();
        String[] exp1=str.split(" ");
        for (int i = 0; i < exp1.length; i++) {
            queue.push(exp1[i]);
        }
        int nZhangduanyun = queue.size();
        System.out.println("The queue :" + queue.toString());
        System.out.println("The size : "+nZhangduanyun);

        //从磁盘读取一个文件， 这个文件有两个数字。
        BufferedInputStream in = new BufferedInputStream(new FileInputStream("C:\\Users\\lenovo\\Desktop\\Exp2.txt"));
        byte[] bytes = new byte[2048];
        int n = -1;
        String a = null;
        while ((n = in.read(bytes,0,bytes.length)) != -1) {
            a = new String(bytes,0,n, "UTF-8");
        }
        StringTokenizer st=new StringTokenizer(a," ");
        int i=0;
        String[] FileNum = new String[2];
        while(st.hasMoreTokens()) {
            FileNum[i]=st.nextToken();
            //System.out.println(FileNum[i]);
            i++;
        }
        System.out.println("FileRead:"+a);
        System.out.println("FileNum1:"+FileNum[0]);
        System.out.println("FileNum2:"+FileNum[1]);

        //从文件中读入数字1,插入到链表第5位,并打印所有数字,元素的总数。
        queue.addMiddle(5,FileNum[0]);
        System.out.println("LinkedList1: "+queue.toString());
        System.out.println("LinkedListSize1: "+queue.size());

        //从文件中读入数字2,插入到链表第0位,并打印所有数字&元素的总数。
        queue.addFirst(FileNum[1]);
        System.out.println("LinkedList2: "+queue.toString());
        System.out.println("LinkedListSize2: "+queue.size());

        //从链表中删除刚才的数字1,并打印所有数字和元素的总数。
        queue.Delete(6);
        System.out.println("LinkedList3: "+queue.toString());
        System.out.println("LinkedListSize3: "+queue.size());


    }
}
