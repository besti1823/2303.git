package Chapter14.StackArray;

public interface StackADT<T> {
    public void push (T element);

    public void expandCapacity();

    public T pop();

    public T peek();

    public boolean isEmpty();

    public int size();

    @Override
    public String toString();
    public void addFirst(T element);

    void addMiddle(int index, T element);

    public void Delete(int index);
}