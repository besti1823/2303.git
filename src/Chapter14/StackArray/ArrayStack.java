package Chapter14.StackArray;

import java.util.Arrays;

public class ArrayStack<T> implements StackADT<T>
{
    private final static int DEFAULT_CAPACITY = 100;

    private int top;
    private T[] stack;

    public ArrayStack()
    {
        this(DEFAULT_CAPACITY);
    }

    public ArrayStack(int initialCapacity)
    {
        top = 0;
        stack = (T[])(new Object[initialCapacity]);
    }

    @Override
    public void push(T element)
    {
        if (size() == stack.length)
            expandCapacity();

        stack[top] = element;
        top++;
    }


    @Override
    public void expandCapacity()
    {
        stack = Arrays.copyOf(stack, stack.length * 2);
    }


    @Override
    public T pop() throws EmptyCollectionException
    {
        if (isEmpty()) {
            throw new EmptyCollectionException("stack");
        }

        top--;
        T result = stack[top];
        stack[top] = null;

        return result;
    }


    @Override
    public T peek() throws EmptyCollectionException
    {
        if (isEmpty()) {
            throw new EmptyCollectionException("stack");
        }

        return stack[top-1];
    }


    @Override
    public boolean isEmpty()
    {
        // To be completed as a Programming Project
        if (size() == 0){
            return true;
        }
        else {
            return false;
        }
    }


    @Override
    public int size()
    {
        // To be completed as a Programming Project
        return top;
    }


    @Override
    public String toString()
    {
        // To be completed as a Programming Project
        String str = "";
        for (int i = 0;i < top;i++){
            str += stack[i] + " ";
        }
        return str;
    }

    @Override
    public void addFirst(T element) {

    }

    @Override
    public void addMiddle(int index, T element) {

    }


    @Override
    public void Delete(int index) {

    }

    public void insert(int index,T element){
        if (index != 0){
            int i;
            for (i = top + 1; i >= index - 1; i--){
                stack[i] = stack[i - 1];
            }
            stack[i + 1] = element;
        }
        else {
            for (int i = top + 1;i > 0;i--){
                stack[i] = stack[i - 1];
            }
            stack[0] = element;
        }
        top++;
    }

    public void delete(int index){
        for (int i = index; i < top; i++) {
            stack[i] = stack[i + 1];
        }
        top--;
    }
}
