package Chapter14.StackArray;
public class ArrayStackTest {
        public static void main(String[] args) {
            ArrayStack stack = new ArrayStack();
            stack.push(9);
            stack.push(2);
            stack.push(2000);
            //test:peek、isEmpty、size,toString
            System.out.println("Top:" + stack.peek());
            System.out.println("IsEmpty:" + stack.isEmpty());
            System.out.println("StackSize:" + stack.size());
            System.out.println(stack.toString());
        }
    }
