package Chapter14.LinkedStack;

public class LinearTest {
    public static void main(String[] args) {
        LinkedStack linkedStack = new LinkedStack();
        System.out.print("Is Empty: ");
        System.out.println(linkedStack.isEmpty());
        linkedStack.push(2301);
        linkedStack.push(2302);
        linkedStack.push(2303);
        linkedStack.push(2304);
        System.out.print("Still Empty?: ");
        System.out.println(linkedStack.isEmpty());
        System.out.println("Size: "+linkedStack.size());
        System.out.println("Top is:"+linkedStack.peek());
        linkedStack.pop();
        System.out.println("Top after pop is:"+linkedStack.peek());
        System.out.println("toStringTest:\n" + linkedStack.toString());
    }
}
