package Chapter14.LinkShijian;

public class IntegerLinkedListExam{
    public static void main(String[] args) {
        //（1）通过键盘输入一些整数，建立一个链表
        IntegerLinkedlist num1 = new IntegerLinkedlist(20);
        IntegerLinkedlist num2 = new IntegerLinkedlist(18);
        IntegerLinkedlist num3 = new IntegerLinkedlist(23);
        IntegerLinkedlist num4 = new IntegerLinkedlist(3);
        IntegerLinkedlist num5 = new IntegerLinkedlist(9);
        IntegerLinkedlist num6 = new IntegerLinkedlist(2);

        IntegerLinkedlist Head = num1;
        InsertNode(Head, num2);
        InsertNode(Head, num3);
        InsertNode(Head, num4);
        InsertNode(Head, num5);
        InsertNode(Head, num6);

        System.out.println("LinkedList Input: ");
        PrintLinkedlist(Head);
        System.out.println();
        System.out.println("Bubble sort: ");
        //selectionSort(Head);
        bubbleSort(Head);
        PrintLinkedlist(Head);
        System.out.println();
        System.out.println("Delete the tail: ");
        Deletenode(Head,num6);
        PrintLinkedlist(Head);
        System.out.println();
        System.out.println("Tail to Head:");
        InsertNode1(Head,num6);
        PrintLinkedlist(Head);
    }

    //（2）实现节点插入、删除、输出等操作
    public static void PrintLinkedlist(IntegerLinkedlist Head) {
        IntegerLinkedlist node = Head;
        while (node != null) {
            System.out.print(" " + node.number);
            node = node.next;
        }
    }

    //头插法
    public static void InsertNode1(IntegerLinkedlist Head, IntegerLinkedlist node) {
        node.next = Head;
        Head = node;
        System.out.print("" + Head.number);
    }

    //尾插法
    public static void InsertNode(IntegerLinkedlist Head, IntegerLinkedlist node) {
        IntegerLinkedlist temp = Head;
        //遍历链表，找到链表末尾。
        while (temp.next != null) {
            temp = temp.next;
        }
        temp.next = node;
    }

    //中间插入。
    public static void InsertNode(IntegerLinkedlist Head, IntegerLinkedlist node1, IntegerLinkedlist node2) {
        IntegerLinkedlist point = Head;
        while ((point.number != node1.number) & point != null) {
            point = point.next;
        }
        if (point.number == node1.number) {
            node1.next = point.next;
            point.next = node2;
        }
    }

    //删除方法。
    public static void Deletenode(IntegerLinkedlist Head, IntegerLinkedlist node) {//这里的node要指向需要删除对象的前一个节点。
        IntegerLinkedlist prenode = Head, currentnode = Head;
        while (prenode != null) {
            if (currentnode != node) {
                prenode = currentnode;
                currentnode = currentnode.next;
            } else {
                break;
            }
        }
        prenode.next = currentnode.next;
    }

    //使用冒泡排序法或者选择排序法根据数值大小对链表进行排序。
    //选择排序法。
    public static IntegerLinkedlist selectionSort(IntegerLinkedlist Head) {
        //记录每次循环的最小值
        int temp ;
        IntegerLinkedlist curNode = Head;
        while (curNode != null) {
            /**
             * 内重循环从当前节点的下一个节点循环到尾节点，
             * 找到和外重循环的值比较最小的那个，然后与外重循环进行交换
             */
            IntegerLinkedlist nextNode = curNode.next;
            while (nextNode != null) {
                //比外重循环的小值放到前面
                if (nextNode.number < curNode.number) {
                    temp = nextNode.number;
                    nextNode.number = curNode.number;
                    curNode.number = temp;
                }
                nextNode = nextNode.next;
            }
            curNode = curNode.next;
        }
        return Head;
    }

    //冒泡排序法。
    public static IntegerLinkedlist bubbleSort(IntegerLinkedlist Head){
        if(Head == null || Head.next == null)  //链表为空或者仅有单个结点
            return Head;
        IntegerLinkedlist current = null, tail = null;
        current = Head;

        while(current.next != tail){
            while(current.next != tail){
                if(current.number > current.next.number){
                    int temp = current.number;
                    current.number = current.next.number;
                    current.next.number = temp;
                }
                current = current.next;
            }

            tail = current;
            current = Head;
        }

        return Head;
    }
}

