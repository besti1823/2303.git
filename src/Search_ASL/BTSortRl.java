package Search_ASL;
import java.util.LinkedList;
import java.util.Queue;
public class BTSortRl<Key extends Comparable<Key>, Value> {
    private Node root;
    private class Node {
        private Key key;
        private Value value;
        private Node left, right;

        public Node(Key key, Value value) {
            this.key = key;
            this.value = value;
        }
        public String toString() {
            return "[" + key + "," + value + "]";
        }
    }

    public void put(Key key, Value value) {
        root = put(root, key, value);
    }

    private Node put(Node h, Key key, Value value) {
        if (h == null) return new Node(key, value);
        int cmp = key.compareTo(h.key);

        if (cmp < 0) h.left = put(h.left, key, value);  //左子树
        else if (cmp > 0) h.right = put(h.right, key, value); //右子树
        else h.value = value;

        return h;
    }

    public void layerTraverse() {
        layerTraverse(root);
    }
    private void layerTraverse(Node h) {
        if (h == null) return;
        Queue<Node> queue = new LinkedList<Node>();
        queue.add(h);
        while (!queue.isEmpty()) {
            Queue<Node> tmp = new LinkedList<Node>();
            while (!queue.isEmpty()) {
                Node cur = queue.poll();
                System.out.print(cur + " ");
                if (cur != null) {
                    tmp.add(cur.left);
                    tmp.add(cur.right);
                }
            }
            queue = tmp;
            System.out.println();
        }
    }


    public Value get(Key key) {
        return get(root, key);
    }

    public Value get(Node h, Key key) {
        if (h == null) return null;
        int cmp = key.compareTo(h.key);

        if (cmp < 0) return get(h.left, key);
        else if (cmp > 0) return get(h.right, key);
        else return h.value;
    }

    public void deleteMin () {
        root = deleteMin(root);
    }

    public Node deleteMin(Node h) {
        if (h == null) return null;
        if (h.left == null) return h.right;
        h.left = deleteMin(h.left);
        return h;
    }


    private Node min (Node h) {
        if (h == null) return null;
        while (h.left != null) h = h.left;
        return h;
    }

    public void delete (Key key) {
        root = delete(root, key);
    }

    private Node delete(Node h, Key key) {
        if (h == null) return null;
        int cmp = key.compareTo(h.key);

        if (cmp < 0) h.left = delete(h.left, key);
        else if (cmp > 0) h.right = delete(h.right, key);
        else {
            if (h.left == null) return h.right;
            if (h.right == null) return h.left;

            Node min_of_h_right = min(h.right);
            Node root_of_h_right = deleteMin(h.right);
            min_of_h_right.right = root_of_h_right;
            min_of_h_right.left = h.left;
            h = min_of_h_right;
        }
        return h;
    }

    public static void main(String[] args) {
        BTSortRl<String, Integer> bts = new BTSortRl<String, Integer>();
        bts.put("A",20);
        bts.put("B",18);
        bts.put("C",23);
        bts.put("D",3);
        bts.put("E",9);
        bts.put("F",2);
        bts.put("G",18);
        System.out.println("-------BinaryTree1-------");
        bts.layerTraverse();
        System.out.println("\n-------Get \"C\"-------");
        System.out.println(bts.get("C"));
        System.out.println("\n-------Get \"D\"-------");
        System.out.println(bts.get("D"));

        bts.delete("E");
        System.out.println("-------BinaryTree2-------");
        bts.layerTraverse();

        bts.deleteMin();
        System.out.println("-------BinaryTree3-------");
        bts.layerTraverse();
    }

}
