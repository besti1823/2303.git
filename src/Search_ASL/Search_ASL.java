package Search_ASL;
//顺序查找、折半查找、二叉排序树查找、散列查找(用线性探查法和链地址法)
public class Search_ASL {
    //顺序查找
    public static int LinearSearch(int[] a, int key) {
        for (int i = 0, length = a.length; i < length; i++) {
            if (a[i] == key)
                return i;
        }
        return -1;
    }
    //顺序查找优化
    public static int LinearSearchPro(int[] a, int key) {
        int index = a.length - 1;
        if (key == a[index])
            return index;
        a[index] = key;
        int i = 0;
        while (a[i++] != key) ;
        return i == index + 1 ? -1 : i - 1;
    }
    //折半查找（递归实现）
    public static int binarySearch(int[] array, int value) {
        int low = 0;
        int high = array.length - 1;
        return searchMy(array, low, high, value);
    }
    private static int searchMy(int array[], int low, int high, int value) {
        if (low > high)
            return -1;
        int mid = low + ((high - low) >> 1);
        if (value == array[mid])
            return mid;
        if (value < array[mid])
            return searchMy(array, low, mid - 1, value);
        return searchMy(array, mid + 1, high, value);
    }
    //二叉排序树查找——BTSortRl
    //散列查找：线性探查法

    //链地址法
}
