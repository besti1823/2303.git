package Search_ASL;

public class SeperateChainingHashSet<K,V> {
        public int num; //当前散列表中的键值对总数
        public int capacity; //散列表的大小
        public SeqSearchST<K, V>[] st; //链表对象数组
        public SeperateChainingHashSet(int initialCapacity) {
            capacity = initialCapacity;
            st = (SeqSearchST<K, V>[]) new Object[capacity];
            for (int i = 0; i < capacity; i++) {
                st[i] = new SeqSearchST<K, V>();
            }
        }
        private int hash(K key) {
            return (key.hashCode() & 0x7fffffff) % capacity;
        }
        public V get(K key) {
            return st[hash(key)].get(key);
        }
        public void put(K key, V value) {
            st[hash(key)].put(key, value);
        }
}

