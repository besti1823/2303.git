package MyGragh;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class UnGraph {
        private String[] vexs = null;
        //点集信息
        private int[][] arcs = null;
        //边集信息
        private boolean[] visited = null;
    public UnGraph(int vexNum,int arcNum) {
        //输入点的个数和边的个数

        this.vexs = new String[vexNum];
        this.arcs = new int[vexNum][vexNum];

        System.out.print("请依次输入顶点值，以空格隔开：");
        Scanner sc = new Scanner(System.in);
        for(int i = 0; i < vexNum; i++) {
            //根据输入建立点集
            this.vexs[i] = sc.next();
        }

        for(int i = 0; i < vexNum; i++) {
            //初始化边集
            for(int j = 0; j < vexNum; j++) {
                this.arcs[i][j] = 0;
                //0表示该位置所对应的两顶点之间没有边
            }
        }

        start:for(int i = 0; i < arcNum; i++) {
            //开始建立边集

            sc = new Scanner(System.in);
            int vex1Site = 0;
            int vex2Site = 0;
            String vex1 = null;
            String vex2 = null;

            System.out.print("请输入第" + (i+1) + "条边所依附的两个顶点，以空格隔开：");
            vex1 = sc.next();
            vex2 = sc.next();
            for(int j = 0; j < this.vexs.length; j++) {
                //查找输入的第一个顶点的位置
                if (this.vexs[j].equals(vex1)) {
                    vex1Site = j;
                    break;
                }
                if (j == this.vexs.length - 1) {
                    System.out.println("未找到第一个顶点，请重新输入！");
                    i--;
                    continue start;
                }
            }
            for (int j = 0; j < this.vexs.length; j++) {
                //查找输入的第二个顶点的位置
                if(this.vexs[j].equals(vex2)) {
                    vex2Site = j;
                    break;
                }
                if (j == this.vexs.length - 1) {
                    System.out.println("未找到第二个顶点，请重新输入！");
                    i--;
                    continue start;
                }
            }
            if(this.arcs[vex1Site][vex2Site] != 0) {
                //检测该边是否已经输入
                System.out.println("该边已存在！");
                i--;
                continue start;
            }else {
                this.arcs[vex1Site][vex2Site] = 1;
                //1表示该位置所对应的两顶点之间有边
                this.arcs[vex2Site][vex1Site] = 1;
                //对称边也置1
            }
        }
        System.out.println("基于邻接矩阵的无向图创建成功！");
        sc.close();
    }

    public void dFSTraverse() {

        this.visited = new boolean[this.vexs.length];
        //初始化访问标志数组
        for(int i = 0; i < this.visited.length; i++) {
            this.visited[i] = false;
        }

        for(int i = 0; i < this.visited.length; i++) {
            if(!this.visited[i]) {
                //对未访问的顶点调用深度优先遍历算法
                dFS_AM(i);
            }
        }
    }
    public void dFS_AM(int site) {
        //输入深度优先遍历的开始顶点
        System.out.println(this.vexs[site]);
        //输出该顶点
        this.visited[site] = true;
        //置访问标志为true
        for(int i = 0; i < this.vexs.length; i++) {
            //依次查找未访问邻接点，并以该邻接点为始点调用深度优先遍历算法
            if(this.arcs[site][i] != 0 && !this.visited[i]) {
                this.dFS_AM(i);
            }
        }
    }
    public void bFSTraverse() {

        this.visited = new boolean[this.vexs.length];
        //初始化访问标志数组
        for(int i = 0; i < this.visited.length; i++) {
            this.visited[i] = false;
        }

        for(int i = 0; i < this.visited.length; i++) {
            if(!this.visited[i]) {
                //对未访问的顶点调用广度优先遍历算法
                bFS_AM(i);
            }
        }
    }
    public void bFS_AM(int site) {
        //输入开始顶点
        System.out.println(this.vexs[site]);
        //输出该顶点
        this.visited[site] = true;
        //置访问标志为true
        LinkedList<Integer> linkedList = new LinkedList<Integer>();
        //借助队列来实现广度优先遍历
        linkedList.offer(site);
        //将访问过的顶点入队
        while(!linkedList.isEmpty()) {
            int vexSite = linkedList.poll();
            //队头顶点出队
            for(int i = 0; i < this.vexs.length; i++) {
                if(this.arcs[vexSite][i] != 0 && !this.visited[i]) {
                    //依次查找未访问的邻接点进行访问后入队
                    System.out.println(this.vexs[i]);
                    this.visited[i] = true;
                    linkedList.offer(i);
                }
            }
        }
    }
}

