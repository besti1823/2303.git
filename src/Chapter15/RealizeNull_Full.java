package Chapter15;

import Chapter14.StackArray.EmptyCollectionException;

public class RealizeNull_Full {
    public static void main(String[] args) throws EmptyCollectionException {
        CircularArrayQueue<Integer> c1=new CircularArrayQueue<Integer>();
        CircularArrayQueue<Integer> c2=new CircularArrayQueue<Integer>();

        c1.enqueue(20);
        c1.enqueue(18);
        c1.enqueue(23);
        c1.enqueue(3);
        System.out.println(c1);
        c1.enqueue(92);
        System.out.println("FULL: "+c1);
        c1.dequeue();
        System.out.println(c1);
        System.out.println("isEmpty?"+c1.isEmpty());
        System.out.println("Size: "+c1.size());
        System.out.println("The first one: "+c1.first());
        c1.dequeue();
        c1.dequeue();
        c1.dequeue();
        c1.dequeue();
        System.out.println("NULL: "+c1);
    }
}

