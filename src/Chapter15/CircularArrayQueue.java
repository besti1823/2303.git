package Chapter15;

import Chapter14.StackArray.EmptyCollectionException;

public class CircularArrayQueue<T> implements Queue<T>{
    private final int DEFAULT_CAPACITY=10;
    private int front,rear,count;
    private T[] queue;

    public CircularArrayQueue(){
        front=rear=count=0;
        queue=(T[])(new Object[DEFAULT_CAPACITY]);
    }

    @Override
    public void enqueue(T element) {
        if(count==queue.length)
            expandCapacity();
        queue[rear]=element;
        rear=(rear+1)%queue.length;
        count++;
    }

    public void expandCapacity(){
        T[] larger=(T[])(new Object[queue.length*2]);

        for(int index=0;index<count;index++)
            larger[index]=queue[(front+index)%queue.length];
        front=0;
        rear=count;
        queue=larger;
    }

    @Override
    public T dequeue() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("queue");
        }
        T result = queue[front];
        queue[front] = null;
        front = (front + 1) % queue.length;  //取余赋值确定下一个出队位置
        count--;
        return result;
    }

    @Override
    public T first() throws EmptyCollectionException{
        if (isEmpty()) {
            throw new EmptyCollectionException("queue");
        }
        return queue[front];
    }

    @Override
    public boolean isEmpty() {
        return count==0;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String toString() {
        String result = "";
        int scan = 0;

        while (scan < count) {
            if (queue[scan] != null) {
                result += queue[scan].toString() + "\n";
            }
            scan++;
        }
        return result;
    }
}
