package Chapter15;

import Chapter14.LinkedStack.LinearNode;
import Chapter14.StackArray.EmptyCollectionException;

public class LinkedQueue<T> implements Queue<T> {
    private int count;
    private LinearNode<T> front,rear;
    public LinkedQueue(){
        count=0;
        front=rear=null;
    }

    public void enqueue(T element) {
        LinearNode<T> node =new LinearNode<T>(element);

        if(count==0)
            front=node;
        else
            rear.setNext(node);

        rear=node;
        count++;
    }

    @Override
    public T dequeue() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("The queue is empty.");
        }
        T result = front.getElement();
        front = front.getNext();
        count--;
        if (isEmpty()) {
            rear = null;
        }
        return result;
    }

    @Override
    public T first() throws  EmptyCollectionException{
        if (isEmpty()){
            throw new EmptyCollectionException ("The queue is empty.");
        }
        return front.getElement();
    }

    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public String toString() {
        String result = "";
        LinearNode<T> current = front;
        while (current != null)
        {
            result += current.getElement().toString() + "\n";
            current = current.getNext();
        }
        return result;
    }
}
