package Calculatorzdy;

import java.util.StringTokenizer;

class Calculator {
    int numerator;  // 分子
    int denominator; // 分母


    public Calculator(int a, int b) {
        if (a == 0) {
            numerator = 0;
            denominator = 1;
        } else {
            setnumeratoranddenominator(a, b);
        }
    }

    public Calculator() {}

    void setnumeratoranddenominator(int a, int b) {  // 设置分子和分母
        int c = f(Math.abs(a), Math.abs(b));         // 计算最大公约数
        numerator = a / c;
        denominator = b / c;
        if (numerator < 0 && denominator < 0) {
            numerator = -numerator;
            denominator = -denominator;
        }
    }
    public int getnumerator(){
        return numerator;
    }
    public int getdenominator(){
        return denominator;
    }
    int f(int a, int b) {  // 求a和b的最大公约数
        if (a < b) {
            int c = a;
            a = b;
            b = c;
        }
        int r = a % b;
        while (r != 0) {
            a = b;
            b = r;
            r = a % b;
        }
        return b;
    }
    public Calculator add(Calculator r) {  // 加法运算
        int a = r.getnumerator();
        int b = r.getdenominator();
        int newnumerator = numerator * b + denominator * a;
        int newdenominator = denominator * b;
        Calculator result = new Calculator(newnumerator, newdenominator);
        return result;
    }
    public Calculator sub(Calculator r) {  // 减法运算
        int a = r.getnumerator();
        int b = r.getdenominator();
        int newnumerator = numerator * b - denominator * a;
        int newdenominator = denominator * b;
        Calculator result = new Calculator(newnumerator, newdenominator);
        return result;
    }
    public Calculator multi(Calculator r) { // 乘法运算
        int a = r.getnumerator();
        int b = r.getdenominator();
        int newnumerator = numerator * a;
        int newdenominator = denominator * b;
        Calculator result = new Calculator(newnumerator, newdenominator);
        return result;
    }
    public Calculator div(Calculator r) {  // 除法运算
        int a = r.getnumerator();
        int b = r.getdenominator();
        int newnumerator = numerator * b;
        int newdenominator = denominator * a;
        Calculator result = new Calculator(newnumerator, newdenominator);
        return result;
    }

    // 封装了具体运算，主要为对输入进行转换，对输出封装

    public static int[] compute(String data1, String operation, String data2) {
        int[] r=new int[2];
        StringTokenizer step = new StringTokenizer(data1, "/");
        int data1_1 = Integer.parseInt(step.nextToken());
        int data1_2 = Integer.parseInt(step.nextToken());
        step = new StringTokenizer(data2, "/");
        int data2_1 = Integer.parseInt(step.nextToken());
        int data2_2 = Integer.parseInt(step.nextToken());

        Calculator r1 = new Calculator(data1_1, data1_2);
        Calculator r2 = new Calculator(data2_1, data2_2);

        Calculator result;
        int a = 0, b = 0;
        if (operation.equals("+")) {
            result = r1.add(r2);
            a = result.getnumerator();
            b = result.getdenominator();
        }

        if (operation.equals("-")) {
            result = r1.sub(r2);
            a = result.getnumerator();
            b = result.getdenominator();
        }

        if (operation.equals("*")) {
            result = r1.multi(r2);
            a = result.getnumerator();
            b = result.getdenominator();
        }

        if (operation.equals("/")) {
            result = r1.div(r2);
            a = result.getnumerator();
            b = result.getdenominator();
        }
        r[0]=a;
        r[1]=b;
        return r;
    }
}