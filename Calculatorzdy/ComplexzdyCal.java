package Calculatorzdy;

public class ComplexzdyCal implements Complexzdy {
    private double a,b,c,d;

    public ComplexzdyCal(double a,double b,double c,double d){
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    @Override
    public String CAdd() {
        double R,I;
        R = a + c;
        I = b + d;
        String s = "";
        if (I > 0)
            s = R + "+" + I + "i";
        else if (I < 0)
            s = R + "" + I + "i";
        else
            s = R + "";
        return s;
    }

    @Override
    public String CSub() {
        double R,I;
        R = a - c;
        I = b - d;
        String s = "";
        if (I > 0)
            s = R + "+" + I + "i";
        else if (I < 0)
            s = R + "" + I + "i";
        else
            s = R + "";
        return s;
    }

    @Override
    public String CMulti() {
        double R,I;
        R = a * c - b * d;
        I = b * c + a * d;
        String s = "";
        if (I > 0)
            s = R + "+" + I + "i";
        else if (I < 0)
            s = R + "" + I + "i";
        else
            s = R + "";
        return s;
    }

    @Override
    public String CDiv() {
        double R,I;
        double t = c * c + d * d;
        R = (a * c + b * d) / t;
        I = (b * c - a * d) / t;
        String s = "";
        if (I > 0)
            s = R + "+" + I + "i";
        else if (I < 0)
            s = R + "" + I + "i";
        else
            s = R + "";
        return s;
    }

    @Override
    public String CCompare() {
        String ss,s1,s2 = null;
        double m1 = Math.sqrt(a*a+b*b);
        double m2 = Math.sqrt(c*c+d*d);
        if (b > 0)
            s1 = a + "+" + b + "i";
        else if (b < 0)
            s1 = a + "" + b + "i";
        else
            s1 = a + "";
        if (d > 0)
            s2 = c + "+" + d + "i";
        else if (d < 0)
            s2 = c + "" + d + "i";
        else
            s2 = a + "";
        if(m1>m2)
            ss = s1+" > "+s2;
        else if(m1<m2)
            ss = s1+" < "+s2;
        else
            ss = s1+" = "+s2;
        return ss;
    }
}