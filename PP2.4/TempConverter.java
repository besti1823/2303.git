/*================================================================
*   Copyright (C) 2019Zhang_duanyun. All rights reserved.
*   
*   文件名称：TempConverter.java
*   创 建 者：Zhang_duanyun
*   创建日期：2019年09月15日
*   描    述：
*
================================================================*/

import java.util.Scanner;

public class TempConverter
{
	public static void main (String[] args)
	{
		double f,c;

		Scanner scan = new Scanner(System.in);

		System.out.println ("Enter the Fahrenheit:");
		f = scan.nextInt();
		c = (f-32)/1.8;

		System.out.println ("The Celsius: "+c);
	}
}
