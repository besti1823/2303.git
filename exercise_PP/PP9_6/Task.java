package PP9_6;

public class Task implements Priority {
    private int priority;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Task(int priority,String name) {
        this.priority = priority;
        this.name = name;
    }

    @Override
    public void setPriority() {
        this.priority = priority;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    @Override
    public String toString() {
        return "Task{" +
                "priority=" + priority +
                ", name='" + name + '\'' +
                '}';
    }
}
