package PP9_2;

public class Volunteer extends StaffMember
{

    public Volunteer (String eName, String eAddress, String ePhone,int eGrade)
    {
        super (eName, eAddress, ePhone,eGrade);
    }

    public double pay()
    {
        return 0.0;
    }

    @Override
    public void holiday() {
        int op = grade;
        switch (op) {
            case 1:
                System.out.println("Holiday: 10");
                break;
            case 2:
                System.out.println("Holiday: 9");
                break;
            case 3:
                System.out.println("Holiday: 8");
                break;
            case 4:
                System.out.println("Holiday: 7");
                break;
            default:
                break;
        }
    }
}
