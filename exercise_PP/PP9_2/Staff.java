package PP9_2;

public class Staff
{
    private StaffMember[] staffList;

    public Staff ()
    {
        staffList = new StaffMember[6];

        staffList[0] = new Executive("Zdy","123 Main Line","18811355692",3,"20182303",2423.07);
        staffList[1] = new PP9_2.Employee("Lcz","456 Off  Line","188lcispig1",4,"20182311",1246.15);
        staffList[2] = new PP9_2.Employee("Xry","789 Off Rocker","18888888888",3,"20182399",1169.23);
        staffList[3] = new PP9_2.Hourly("Jack","678 Fifth Ave.","18866666666",3,"20182366",10.);
        staffList[4] = new PP9_2.Volunteer("Rose","987 Babe Blvd","18812341234",4);
        staffList[5] = new PP9_2.Volunteer("Ben","321 Dud Lane","18843214321",5);

        ((Executive)staffList[0]).awardBonus (500.00);

        ((Hourly)staffList[3]).addHours (40);
    }


    public void info ()
    {
        double amount;

        for (int count=0; count < staffList.length; count++)
        {
            System.out.println (staffList[count]);

            amount = staffList[count].pay();  // polymorphic

            if (amount == 0.0)
                System.out.println ("Thanks!");
            else
                System.out.println ("Paid: " + amount);
            staffList[count].holiday();
            System.out.println ("-----------------------------------");
        }
    }

}
