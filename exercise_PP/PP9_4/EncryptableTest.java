package PP9_4;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

public class EncryptableTest
{

    public static void main (String[] args) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        Encryptable hush;

        hush = new Secret("ZDY is Studying! ");
        System.out.println("Secret:"+hush);
        hush.encrypt();
        System.out.println ("Encryption:"+hush);
        hush.decrypt();
        System.out.println ("Decryption:"+hush+"\n");

        hush = new Password("HelloWorld");
        System.out.println("Origin:"+hush);
        hush.encrypt();
        System.out.println ("Encryption:"+hush);
        hush.decrypt();
        System.out.println ("Decryption:"+hush);
    }
}

