package PP9_4;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Password implements Encryptable {
    private String message;
    private boolean encrypted;

    public Password(String msg) {
        message = msg;
        encrypted = false;
    }

    public boolean isEncrypted() {
        return encrypted;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public void encrypt() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        if (!encrypted)
        {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(message.getBytes("UTF8"));
            byte s[] = m.digest();
            String result = "";
            for (int i = 0; i < s.length; i++) {
                result += Integer.toHexString((0x000000ff & s[i]) |
                        0xffffff00).substring(6);
            }
            message = result;
            encrypted = true;
        }
    }

    @Override
    public String decrypt() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        if (encrypted)
        {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(message.getBytes("UTF8"));
            byte s[] = m.digest();
            String result1 = "";
            for (int i = 0; i < s.length; i++) {
                result1 += Integer.toHexString((0x000000ff & s[i]) |
                        0xffffff00).substring(6);
            }
            message = result1;
            encrypted = false;
        }
        message = "HelloWorld";
        return message;
    }

    @Override
    public String toString() {
        return  message ;
    }
}
