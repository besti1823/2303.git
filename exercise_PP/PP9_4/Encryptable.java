package PP9_4;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

public interface Encryptable
{
   public void encrypt() throws NoSuchAlgorithmException, UnsupportedEncodingException;
   public String decrypt() throws NoSuchAlgorithmException, UnsupportedEncodingException;
}
