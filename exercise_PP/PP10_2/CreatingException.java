package PP10_2;

import java.util.Scanner;

public class CreatingException {
    public static void main(String[] args) throws StringOverException{
        String str1 = "";
        Scanner scan = new Scanner(System.in);
        StringOverException problem = new StringOverException("This String is too long.");
        while(!str1.equals("DONE"))
        {
        System.out.println("Input string:");
        String str = scan.next();
        if(str.length()>20)
            throw problem;
        System.out.println("Do you want to stop?(Input DONE)");
        str1 = scan.next();
        }
    }
}
