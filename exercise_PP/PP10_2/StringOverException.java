package PP10_2;

public class StringOverException extends Exception{
    public StringOverException(String message) {
        super(message);
    }
}
