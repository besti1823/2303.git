package PP10_3;

import java.util.Scanner;

public class CreatingException {
    public void Exce() throws StringOverException {
        Scanner scan = new Scanner(System.in);
        StringOverException problem = new StringOverException("This String is too long.");
        System.out.println("Input string:");
        String str = scan.next();
        if(str.length()>20)
            throw problem;
    }
}
