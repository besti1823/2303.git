package PP10_3;

public class StringOverException extends Exception{
    public StringOverException(String message) {
        super(message);
    }
}
