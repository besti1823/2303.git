/*================================================================
*   Copyright (C) 2019Zhang_duanyun. All rights reserved.
*   
*   文件名称：book.java
*   创 建 者：Zhang_duanyun
*   创建日期：2019年09月20日
*   描    述：
*
================================================================*/

public class  Book
{
       private String bookname;
       private String author;
       private String press;
       public String zpndate;

       public Book(String bookname,String author,String press,String zpndate)//accept the value of the main function
       {
           this.setBookname(bookname);  
           this.setAuthor(author);  
           this.setPress(press);  
           this.setZpndate(zpndate);  
       }//via set and get private to public
       public void setBookname(String bookname)
       {
           this.bookname = bookname;
       }
       public String getBookname()
       {
           return bookname;
       }
       public void setAuthor(String author)
       {
          this.author = author;
       }
       public String getAuthor()
       {
          return author;
       }
       public void setPress(String press)
       {
          this.press = press;
       }
       public String getPress()
       {
          return press;
       }
       public void setZpndate(String zpndate)
       {
          this.zpndate = zpndate;
       }
       public String getZpndate()
       {
          return zpndate;
       }
       public String toString()
       {
          return "Bookname:"+this.getBookname()+"\n"+"Author:"+this.getAuthor()+"\n"+"Press:"+this.getPress()+"\n"+"Zpn date:"+this.getZpndate();//return a ju hua
       }
}
