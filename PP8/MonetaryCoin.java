package PP8;

public class MonetaryCoin extends ch05.coin.test.Coin
{
  private int facevalue;

  public MonetaryCoin(int facevalue)
  {
    this.facevalue = facevalue;    
  }

  public int getFaceValue()
  {
    return this.facevalue;
  }
}
