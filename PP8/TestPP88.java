package PP8;
public class TestPP88
{
  public static void main (String[] args)
  {
    Mathematical math1 = new Mathematical("高数", "孟璀", "未知",60);
    CourseSystem math = math1;

    System.out.println ("课程名：" + math.getName());
    System.out.println ("教师：" + math.getTeacher());
    System.out.println ("所属院系：" + math.getDepartment());
    System.out.println ("学时：" + math.getClasshour());
  }
}
