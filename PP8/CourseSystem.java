package PP8;

public class CourseSystem
{
  public String name, teacher, department;
  public int classhour;

  public CourseSystem(String name, String teacher, String department, int classhour)
  {
    this.name = name;
    this.teacher = teacher;
    this.department = department;
    this.classhour = classhour;
  }

  public String getName()
  {
    return this.name;
  }

  public String getTeacher()
  {
    return this.teacher;
  }
  
  public String getDepartment()
  {
    return this.department;
  }

  public int getClasshour()
  {
    return this.classhour;
  }
}
