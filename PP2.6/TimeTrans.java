/*================================================================
*   Copyright (C) 2019Zhang_duanyun. All rights reserved.
*   
*   文件名称：TimeTrans.java
*   创 建 者：Zhang_duanyun
*   创建日期：2019年09月22日
*   描    述：
*
================================================================*/

import java.util.Scanner;

public class TimeTrans
{
	public static void main(String[] args)
	{
		int hour,minute,second,time;

		Scanner scan = new Scanner (System.in);
		System.out.println ("Enter the hour: ");
		hour = scan.nextInt();
		System.out.println ("Enter the minute: ");
		minute = scan.nextInt();
		System.out.println ("Enter the second: ");
		second = scan.nextInt();

		time = hour*3600+minute*60+second;

		System.out.println ("The time is "+time+"second.");
	}
}
