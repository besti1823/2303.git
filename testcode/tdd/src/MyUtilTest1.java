/*================================================================
*   Copyright (C) 2019Zhang_duanyun. All rights reserved.
*   
*   文件名称：MyUtilTest1.java
*   创 建 者：Zhang_duanyun
*   创建日期：2019年09月16日
*   描    述：
*
================================================================*/

import java.util.Scanner;
import java.org.junit.Test;
import junit.framework.TestCase;

public class MyUtilTest1 extends TestCase {
            @Test
                public void testNormal() {
                        assertEquals(3,Calculator.add(1,2));
                        assertEquals(-1,Calculator.jian(1,2));
                        assertEquals(2,Calculator.cheng(1,2));
                        assertEquals(2,Calculator.chu(2,1));
                        assertEquals(1,Calculator.mo(1,2));
                      }
}

