/*================================================================
*   Copyright (C) 2019Zhang_duanyun. All rights reserved.
*   
*   文件名称：Calculator.java
*   创 建 者：Zhang_duanyun
*   创建日期：2019年09月16日
*   描    述：
*
================================================================*/

import java.util.Scanner;

public class Calculator
{
	public static void main (String[] args)
	{
		double num1,num2;
		char ope,ch;

		Scanner scan = new Scanner(System.in);

		do{
		System.out.println ("Enter the num1:");
		num1 = scan.nextDouble();
		System.out.println ("Enter the num2:");
		num2 = scan.nextDouble();

		System.out.println ("Enter the operator:");
		String s = scan.next();
		ope = s.charAt(0);

		switch (ope)
		{
			case '*':
				System.out.println ("num1*num2="+(num1*num2));
				break;
			case '/':
                if(num2==0)
				System.out.println ("error:num2 = 0!");
				else
				System.out.println ("num1/num2="+(num1/num2));
                break;
			case '+':
				System.out.println ("num1+num2="+(num1+num2));
			    break;
			case '-':
				System.out.println ("num1-num2="+(num1-num2));
				break;
			case '%':
				if(num2==0)
                 System.out.println ("error:num2 = 0!");
                 else
                 System.out.println ("num1%num2="+(num1%num2));
                 break;
			default:
				 System.out.println ("Error in identification operator.");
		}
		System.out.println ("continue?(y/n)");
		String ss = scan.next();
		ch = ss.charAt(0);
		}while(ch=='y');
	}
	public double add(double a,double b){
               return a+b;
    }
    public double jian(double a,double b){
               return a-b;
    }
    public double cheng(double a,double b){
               return a*b;
    }
    public double chu(double a,double b){
               return a/b;
    }
    public double mo(double a,double b){
               return a%b;
    }
}

