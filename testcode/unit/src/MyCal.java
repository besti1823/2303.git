/*================================================================
*   Copyright (C) 2019Zhang_duanyun. All rights reserved.
*   
*   文件名称：MyCal.java
*   创 建 者：Zhang_duanyun
*   创建日期：2019年09月16日
*   描    述：
*
================================================================*/

public class MyCal{
   public static Double calculator(double num1,double num2,char op){
			if(op== '*')
			    return(num1*num2);	
			else if(op =='/')
				return(num1/num2);
			else if(op=='+')
				return(num1+num2);
			else if(op=='-')
				return(num1-num2);
			else if(op =='%')
			    return(num1%num2);
			else
				return 0.0;
}
}
