import java.util.StringTokenizer;
import java.util.Scanner;
public class RationalCal {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        System.out.println("Input:");
        String calcu = input.next();
        String data1 = calcu.substring(0, 3);
        String operation = calcu.substring(3, 4);
        String data2 = calcu.substring(4, 7);
        /*System.out.println(data1);
        System.out.println(operation);
        System.out.println(data2);*/
        // 根据用户输入进行具体运算
        System.out.println("Result:");
        Cal.compute(data1, operation, data2);
    }
}

class  Cal{
    int numerator;  // 分子
    int denominator; // 分母

    public Cal(int a, int b) {
        if (a == 0) {
            numerator = 0;
            denominator = 1;
        } else {
            setnumeratoranddenominator(a, b);
        }
    }

    public Cal() {}

    void setnumeratoranddenominator(int a, int b) {  // 设置分子和分母
        int c = f(Math.abs(a), Math.abs(b));         // 计算最大公约数
        numerator = a / c;
        denominator = b / c;
        if (numerator < 0 && denominator < 0) {
            numerator = -numerator;
            denominator = -denominator;
        }
    }
    public int getnumerator(){
        return numerator;
    }
    public int getdenominator(){
        return denominator;
    }
    int f(int a, int b) {  // 求a和b的最大公约数
        if (a < b) {
            int c = a;
            a = b;
            b = c;
        }
        int r = a % b;
        while (r != 0) {
            a = b;
            b = r;
            r = a % b;
        }
        return b;
    }
    public Cal add(Cal r) {  // 加法运算
        int a = r.getnumerator();
        int b = r.getdenominator();
        int newnumerator = numerator * b + denominator * a;
        int newdenominator = denominator * b;
        Cal result = new Cal(newnumerator, newdenominator);
        return result;
    }
    public Cal sub(Cal r) {  // 减法运算
        int a = r.getnumerator();
        int b = r.getdenominator();
        int newnumerator = numerator * b - denominator * a;
        int newdenominator = denominator * b;
        Cal result = new Cal(newnumerator, newdenominator);
        return result;
    }
    public Cal multi(Cal r) { // 乘法运算
        int a = r.getnumerator();
        int b = r.getdenominator();
        int newnumerator = numerator * a;
        int newdenominator = denominator * b;
        Cal result = new Cal (newnumerator, newdenominator);
        return result;
    }
    public Cal div(Cal r) {  // 除法运算
        int a = r.getnumerator();
        int b = r.getdenominator();
        int newnumerator = numerator * b;
        int newdenominator = denominator * a;
        Cal result = new Cal(newnumerator, newdenominator);
        return result;
    }

        // 封装了具体运算，主要为对输入进行转换，对输出封装

    public static void compute(String data1, String operation, String data2) {
        StringTokenizer step = new StringTokenizer(data1, "/");
        int data1_1 = Integer.parseInt(step.nextToken());
        int data1_2 = Integer.parseInt(step.nextToken());
        step = new StringTokenizer(data2, "/");
        int data2_1 = Integer.parseInt(step.nextToken());
        int data2_2 = Integer.parseInt(step.nextToken());

        Cal  r1 = new Cal (data1_1, data1_2);
        Cal  r2 = new Cal (data2_1, data2_2);

        Cal result;
        int a, b;
        if (operation.equals("+")) {
            result = r1.add(r2);
            a = result.getnumerator();
            b = result.getdenominator();
            System.out.println(data1 + " " + operation + " " + data2 + " = " + a + "/" + b);
        }

        if (operation.equals("-")) {
            result = r1.sub(r2);
            a = result.getnumerator();
            b = result.getdenominator();
            System.out.println(data1 + " " + operation + " " + data2 + " = " + a + "/" + b);
        }

        if (operation.equals("*")) {
            result = r1.multi(r2);
            a = result.getnumerator();
            b = result.getdenominator();
            System.out.println(data1 + " " + operation + " " + data2 + " = " + a + "/" + b);
        }

        if (operation.equals("/")) {
            result = r1.div(r2);
            a = result.getnumerator();
            b = result.getdenominator();
            System.out.println(data1 + " " + operation + " " + data2 + " = " + a + "/" + b);
        }
    }
}