import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.StringTokenizer;

public class RationalServer {
    public static void main(String[] args) throws IOException {
        //1.建立一个服务器Socket(ServerSocket)绑定指定端口

        ServerSocket serverSocket = new ServerSocket(8);

        //2.使用accept()方法阻止等待监听，获得新连接
        Socket socket = serverSocket.accept();
        //3.获得输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        //获得输出流
        OutputStream outputStream = socket.getOutputStream();
        PrintWriter printWriter = new PrintWriter(outputStream);
        //4.读取用户输入信息
        String info = null;
        System.out.println("Zdy&LcServer Is Ready!");
        while(!((info = bufferedReader.readLine()) ==null)){
            System.out.println("Zdy&Lc:" + info);
        }
        //给客户一个响应
        String reply="welcome!";
        printWriter.write(reply);
        printWriter.flush();
        Scanner input = new Scanner(System.in);
        printWriter.write("Input:");
        printWriter.flush();
        String calcu = input.next();
        String data1 = calcu.substring(0, 3);
        String operation = calcu.substring(3, 4);
        String data2 = calcu.substring(4, 7);
        /*System.out.println(data1);
        System.out.println(operation);
        System.out.println(data2);*/
        // 根据用户输入进行具体运算
        System.out.println("Result:");
        int[] r=Calculator.compute(data1, operation, data2);
        //String reply ="r[0]+'/'+r[1]";
        String reply1 = r[0]+"/"+r[1];
        printWriter.write(reply1);
        printWriter.flush();
        //5.关闭资源
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
}

class Calculator {
    int numerator;  // 分子
    int denominator; // 分母

    public Calculator(int a, int b) {
        if (a == 0) {
            numerator = 0;
            denominator = 1;
        } else {
            setnumeratoranddenominator(a, b);
        }
    }

    public Calculator() {}

    void setnumeratoranddenominator(int a, int b) {  // 设置分子和分母
        int c = f(Math.abs(a), Math.abs(b));         // 计算最大公约数
        numerator = a / c;
        denominator = b / c;
        if (numerator < 0 && denominator < 0) {
            numerator = -numerator;
            denominator = -denominator;
        }
    }
    public int getnumerator(){
        return numerator;
    }
    public int getdenominator(){
        return denominator;
    }
    int f(int a, int b) {  // 求a和b的最大公约数
        if (a < b) {
            int c = a;
            a = b;
            b = c;
        }
        int r = a % b;
        while (r != 0) {
            a = b;
            b = r;
            r = a % b;
        }
        return b;
    }
    public Calculator add(Calculator r) {  // 加法运算
        int a = r.getnumerator();
        int b = r.getdenominator();
        int newnumerator = numerator * b + denominator * a;
        int newdenominator = denominator * b;
        Calculator result = new Calculator(newnumerator, newdenominator);
        return result;
    }
    public Calculator sub(Calculator r) {  // 减法运算
        int a = r.getnumerator();
        int b = r.getdenominator();
        int newnumerator = numerator * b - denominator * a;
        int newdenominator = denominator * b;
        Calculator result = new Calculator(newnumerator, newdenominator);
        return result;
    }
    public Calculator multi(Calculator r) { // 乘法运算
        int a = r.getnumerator();
        int b = r.getdenominator();
        int newnumerator = numerator * a;
        int newdenominator = denominator * b;
        Calculator result = new Calculator(newnumerator, newdenominator);
        return result;
    }
    public Calculator div(Calculator r) {  // 除法运算
        int a = r.getnumerator();
        int b = r.getdenominator();
        int newnumerator = numerator * b;
        int newdenominator = denominator * a;
        Calculator result = new Calculator(newnumerator, newdenominator);
        return result;
    }

    // 封装了具体运算，主要为对输入进行转换，对输出封装

    public static int[] compute(String data1, String operation, String data2) {
        int[] r=new int[2];
        StringTokenizer step = new StringTokenizer(data1, "/");
        int data1_1 = Integer.parseInt(step.nextToken());
        int data1_2 = Integer.parseInt(step.nextToken());
        step = new StringTokenizer(data2, "/");
        int data2_1 = Integer.parseInt(step.nextToken());
        int data2_2 = Integer.parseInt(step.nextToken());

        Calculator r1 = new Calculator(data1_1, data1_2);
        Calculator r2 = new Calculator(data2_1, data2_2);

        Calculator result;
        int a = 0, b = 0;
        if (operation.equals("+")) {
            result = r1.add(r2);
            a = result.getnumerator();
            b = result.getdenominator();
        }

        if (operation.equals("-")) {
            result = r1.sub(r2);
            a = result.getnumerator();
            b = result.getdenominator();
        }

        if (operation.equals("*")) {
            result = r1.multi(r2);
            a = result.getnumerator();
            b = result.getdenominator();
        }

        if (operation.equals("/")) {
            result = r1.div(r2);
            a = result.getnumerator();
            b = result.getdenominator();
        }
        r[0]=a;
        r[1]=b;
        return r;
    }
}