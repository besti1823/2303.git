import junit.framework.TestCase;
import org.junit.Test;

public class StringBufferDemoTest extends TestCase {
    StringBuffer a = new StringBuffer("StringBuffer");//测试12个字符（<=16）
    StringBuffer b = new StringBuffer("StringBufferStringBuffer");//测试24个字符（>16&&<=34）
    StringBuffer c = new StringBuffer("StringBufferStringBufferStringBuffer");//测试36个字符（>=34）
    @Test
    public void testcharAt() throws Exception{
        assertEquals('S',a.charAt(0));
        assertEquals('g',a.charAt(5));
        assertEquals('r',a.charAt(11));
    }
    @Test
    public void testcapacity() throws Exception{
        assertEquals(28,a.capacity());
        assertEquals(40,b.capacity());
        assertEquals(52,c.capacity());
    }
    @Test
    public void testlength() throws Exception{
        assertEquals(12,a.length());
        assertEquals(24,b.length());
        assertEquals(36,c.length());
    }
    @Test
    public void testindexOf() throws Exception{
        assertEquals(0,a.indexOf("Str"));
        assertEquals(5,a.indexOf("gBu"));
        assertEquals(10,a.indexOf("er"));
    }
}