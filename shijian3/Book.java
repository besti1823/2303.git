/*================================================================
*   Copyright (C) 2019Zhang_duanyun. All rights reserved.
*   
*   文件名称：Book.java
*   创 建 者：Zhang_duanyun
*   创建日期：2019年09月20日
*   描    述：
*
================================================================*/

public class Book
{
       private String bookname;
       private String author;
       private String press;
       public String pubdate;

       public Book(String bookname,String author,String press,String pubdate)
       {
           this.setBookname(bookname);  
           this.setAuthor(author);  
           this.setPress(press);  
           this.setPubdate(pubdate);  
       }
       public void setBookname(String bookname)
       {
           this.bookname = bookname;
       }
       public String getBookname()
       {
           return bookname;
       }
       public void setAuthor(String author)
       {
          this.author = author;
       }
       public String getAuthor()
       {
          return author;
       }
       public void setPress(String press)
       {
          this.press = press;
       }
       public String getPress()
       {
          return press;
       }
       public void setPubdate(String pubdate)
       {
          this.pubdate = pubdate;
       }
       public String getPubdate()
       {
          return pubdate;
       }
       public String toString()
       {
          return "Bookname:"+this.getBookname()+"\n"+"Author:"+this.getAuthor()+"\n"+"Press:"+this.getPress()+"\n"+"Pub date:"+this.getPubdate();
       }
}
