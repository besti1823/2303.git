/*================================================================
*   Copyright (C) 2019Zhang_duanyun. All rights reserved.
*   
*   文件名称：Bookshelf.java
*   创 建 者：Zhang_duanyun
*   创建日期：2019年09月20日
*   描    述：
*
================================================================*/

public class Bookshelf
{
	public static void main (String []args)
   {
      Book book = new Book("Essentials of economics","N.Gregory Mankiw","Peking University Press","2017.3");
      System.out.println (book);
   }
}
