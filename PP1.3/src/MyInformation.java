/*================================================================
*   Copyright (C) 2019Zhang_duanyun. All rights reserved.
*   
*   文件名称：MyInformation.java
*   创 建 者：Zhang_duanyun
*   创建日期：2019年09月07日
*   描    述：
*
================================================================*/

public class MyInformation{
   public static void main(String[] args){
       System.out.println ("MyName:zhangduanyun");
       System.out.println ("MyBirthday:2000.09.02");
       System.out.println ("MyHabit:sleep early");
       System.out.println ("MyFavBook:The Moon and Sixpence");
       System.out.println ("MyFavFilm:Capernaum");
}
}
