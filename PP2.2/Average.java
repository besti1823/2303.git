/*================================================================
*   Copyright (C) 2019Zhang_duanyun. All rights reserved.
*   
*   文件名称：Average.java
*   创 建 者：Zhang_duanyun
*   创建日期：2019年09月15日
*   描    述：
*
================================================================*/

import java.util.Scanner;

public class Average
{
	public static void main (String[] args)
	{
		int num1,num2,num3;
		double aver;

        Scanner scan = new Scanner (System.in);

		System.out.println ("Enter the num1:");
		num1 = scan.nextInt();

		System.out.println ("Enter the num2:");
        num2 = scan.nextInt();

        System.out.println ("Enter the num3:");
        num3 = scan.nextInt();

		aver = (num1+num2+num3)/3.0;

		System.out.println ("The average is "+aver+" .");
	}
}

