abstract class Data{
    abstract public void DisplayValue();
}

class Integer extends Data {
    int value;
    Integer(){
        value = 32767;
    }
    public void DisplayValue(){
        System.out.println(value);
    }
}

class Short extends Data {
    short value;
    Short(){
        value = (short) 32768;
    }
    public void DisplayValue(){
        System.out.println(value);
    }
}

abstract class Factory{
    abstract public Data CreateDataObject();
}

class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}

class Document{
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}

class DoubleFactory extends Factory {
    public Data CreateDataObject(){
        return new Short();
    }
}

public class MyDoc {
    static Document i,d;
    public static void main(String[] args) {
        int a = 20182303%6;
        System.out.println("学号运算结果为："+a);
        if (a==1)
            System.out.println("让系统支持Short类");
        i = new Document(new IntFactory());
        i.DisplayData();
        d = new Document(new DoubleFactory()) ;
        d.DisplayData();
    }
}