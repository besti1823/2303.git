/*================================================================
*   Copyright (C) 2019Zhang_duanyun. All rights reserved.
*   
*   文件名称：Arithmetic.java
*   创 建 者：Zhang_duanyun
*   创建日期：2019年09月11日
*   描    述：
*
================================================================*/

import java.util.Scanner;

public class Arithmetic
{
	public static void main (String[] args)
	{
		int num1,num2,sum,differ,pro;
		double quot;

		Scanner scan = new Scanner (System.in);

		System.out.println ("Enter the num1:");
		num1 = scan.nextInt();

		System.out.println ("Enter the num2:");
		num2 = scan.nextInt();

		sum = num1+num2;
		differ = num1-num2;
		pro = num1*num2;
		quot = (double) num1/num2;

		System.out.println ("The sum is "+sum+" .\nThe difference is "+differ
				+" .\nThe product is "+pro+" .\nThe quotient is "+quot+" .");
	}
}


