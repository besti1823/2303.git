/*================================================================
*   Copyright (C) 2019Zhang_duanyun. All rights reserved.
*   
*   文件名称：Square.java
*   创 建 者：Zhang_duanyun
*   创建日期：2019年09月22日
*   描    述：
*
================================================================*/

import java.util.Scanner;

public class Square
{
	public static void main(String[] args)
	{
		int len,c,s;

		Scanner scan = new Scanner(System.in);
		System.out.println ("Enter the length: ");
		len = scan.nextInt();

		c =4*len;
		s =len*len;

		System.out.println ("The circumference:"+c+"\nThe area:"+s);
	}
}
